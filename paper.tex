% -----------------------------------------------------------------------------------------------------------
% 
% This LaTeX file is intended to serve as an example for using the 'nzsee.cls' file to prepare manuscripts
% for submission to the New Zealand Society for Earthquake Engineering Bulletin.
% 
% Created by
%    Reagan Chandramohan
%    Lecturer
%    Department of Civil and Natural Resources Engineering
%    University of Canterbury
% 
% for
%    The New Zealand Society for Earthquake Engineering
% 
% Version 1.0
% 08-Feb-2017
% 
% -----------------------------------------------------------------------------------------------------------

\documentclass{nzsee}

\title{Template for preparing final manuscripts for publication in the NZSEE Bulletin}
\author{Bulletin Editor\affilnum{1} and Rajesh Dhakal\affilnum{2}}

\affiladdr{1}{Replace this with your Title, Institution, City, Email (at least for the Corresponding Author), NZSEE Membership Status if/where applicable (e.g. Member/Fellow/Life Member). An example is provided below.}
\affiladdr{2}{Corresponding Author, Professor, University of Canterbury, Christchurch, rajesh.shakal@canterbury.ac.nz (Member)}

% Define paper publication information
%\renewcommand\volnum{XX}
%\renewcommand\issuenum{Y}
%\renewcommand\pubmonth{Month}
%\renewcommand\pubyear{Year}
%\renewcommand\submitmonth{Month}
%\renewcommand\submityear{Year}
%\renewcommand\reviewmonth{Month}
%\renewcommand\reviewyear{Year}
%\renewcommand\acceptmonth{Month}
%\renewcommand\acceptyear{Year}
%\setcounter{page}{100}

% The height of the first page footer is automatically expanded by 4mm times the number of author affiliation
% entries. To manually control the footer height, set the \footerexpand dimension.
%\setlength{\footerexpand}{6mm}

% The following lines until the \begin{document} command are used only to format this sample paper.
% Please delete these lines when using this template to write your own paper.
\usepackage{float}
\usepackage{tabu}
\tabulinesep 4pt
\renewcommand\refname{}

\begin{document}

\begin{abstract}
This document provides the template for preparing the final manuscript (both \emph{articles} and \emph{technical notes}) that have been accepted for publication in the Bulletin of the New Zealand Society for Earthquake Engineering.

An abstract is mandatory for all articles/notes.
The abstract should succinctly summarise the objectives and conclusions of the paper in no more than 250 words.
Authors using this document as a template should simply copy the actual abstract paragraphs and paste (in \emph{Keep text only} mode) here replacing these two explanatory paragraphs.
\end{abstract}


\section{Introduction}

This document describes guidelines for preparing the layout of an \emph{article} or a \emph{technical note} that has been reviewed, revised and accepted for publication in the Bulletin of the New Zealand Society for Earthquake Engineering.
Note that this format is required only for the final manuscript after the paper has been accepted for publication following the review process as per the journal policy. 

Articles or technical notes for first review or a subsequent re-review (where requested by the reviewer/s) can be prepared and submitted in single column double spacing format.
Once accepted, all final manuscripts must conform to these formatting guidelines.
For a practice paper authored mostly by practicing engineers, in discussion with the Editor the corresponding author can submit the final manuscript unformatted.
In such cases, the Editorial and Production teams will arrange for the accepted papers to be formatted to conform to these guidelines.

An introduction may vary significantly in size and content, depending on the subject matter of the manuscript.
Topics often briefly described in introductions are purpose of the study, methods used to derive results, previous work, and a sketch of manuscript organization.


\section{Sections, Headers, and Footers}

The title, authors' names, paper processing milestone dates and the abstract are included in the first section of the document (i.e.\ Section 1), which is in a single column format.
The next section (i.e.\ Section 2) starts below the abstract, and is in two-column format.
Tables and figures can either span one or two columns depending on their width.
If necessary, additional sections can be created to accommodate figures and tables that span two columns.
Note full page figures and tables can also be accommodated without creating additional sections (as explained later).
Authors' affiliation must be provided in the footer of the first page.
For all co-authors, the affiliation shall include his/her position title, employer's name, location (city only), email address (mandatory for the corresponding author; optional for other co-authors), and NZSEE membership status (if applicable).
For papers with less/more than two authors, delete the existing lines or enter extra lines as necessary.

The corresponding author should be identified with a ``Corresponding Author'' before the author's affiliation.
For example; the second author is identified as the corresponding author for this template.
The corresponding author should enter the submission, review, and acceptance dates (month and year).
The review date refers to when the first round of review was communicated to the authors; and the accepted date corresponds to when the formatted paper was accepted for publication; so there can be a long gap between these two dates (especially if the paper had to go through more than one round of review).
The dates entered by the authors will be checked by the Editor and may be amended if found different from those in the Editor's record.

The authors need not make any changes to the headers in this document.
The Editor will enter the volume and the issue numbers in the header of the first page the correct page numbers at the top of all pages.


\section{Text}

\subsection{Page Setup}

If this template is not used, the document needs to be setup to result in a similar page layout.
For the first page, both the top and bottom margins are 3cm; whereas for all other pages the top and bottom margins are 2cm and 1.5cm, respectively.
The side (i.e. left and right) margins are 2cm for all pages.
The header and footer are setup to start 1cm from the edges.
Page numbers are to be set differently for the odd and even pages.
For clarity, authors should check the page and header/footer setups in different pages of this template.


\subsection{Main Body}

Font (type, size and emphasis) and paragraph (alignment, spacing etc) for different parts of a typical manuscript (normal text, different level headings, figure/table captions etc) have been standardised in the Styles menu of this document.
Authors can easily assign the desired format to any text using the Style menu.

For example, a Normal paragraph may be converted to a heading by simultaneously pressing the ALT key, the SHIFT key and either the left or the right cursor key (i.e., \(\leftarrow\) or \(\rightarrow\)).
Use the left cursor key for a heading style with the same level as the previous one and the right cursor key for a heading style with the next greater level.
Press the same cursor key again to further reduce or increase the level.

Except for the Title and Authors' names (which are in 14 points Times New Roman font), all other parts should be typed in 9 points Times New Roman font.
The main body of the manuscript is typed single spaced and prepared in the Normal style (available in the Styles Menu) which has a 6 points gap between paragraphs.
Provide only a single space after a full stop when starting a new sentence.

\begin{enumerate}
    \item Use this numbered format for numbered lists.
    \item Copy this line elsewhere in the document but you may need to check that the numbering starts correctly both here and in the new location.
\end{enumerate}

\begin{itemize}
    \item Bulleted lists may also be used (not normally immediately after numbered lists though). The numbers/bullets should be aligned in line with the margin.
    \begin{itemize}
        \item Where second level bullets/numbers are needed, they should be aligned with the text of the first level bullets/numbers.
    \end{itemize}
\end{itemize}


\subsection{Headings}

All headings start after a 12 points gap from the previous text/heading.
The styles required for different levels of headings are shown in Figure 1.
Authors should try not to use the third and fourth level headings, where possible.

\begin{figure}[htb!] % NOTE: Use of the [htb!] options to position floats is recommended.
    \centering
    \includegraphics[width=\columnwidth]{figure1}
    \caption{Styles for different levels of heading.}
\end{figure}


\subsection{References}

Citations to references should be made using numbers in square brackets within the text \cite{Blogg1995}.
The bracketed numbers can be provided either inside a sentence (generally immediately following the reference details used as the subject) or at the end of a sentence.
Some examples follow: (i) \emph{Vertical motions recorded in some earthquakes have been found to have peak accelerations in excess of 2g \cite{Bradley2007}}; (ii) \emph{Jeffery and Peters \cite{Begg1994} have conducted real time pseudo-dynamic tests on several RC bridge piers}; (iii) \emph{According to the NZ Concrete Standard \cite{Standards2004}, buckling of bars can be voided if stirrups are spaced no sparser than six times diameter of longitudinal reinforcement}; (iv) \emph{Several modelling techniques \cite{Wilford2020, Dhakal2008, Dhakal2001, GNS2014} have been reported in literature}.

Citations should be numbered in the order they appear in the text.
Where two references are cited together, separate them by a comma and a space \cite{Dhakal2001, GNS2014}.
If three or more references are to be cited, a hyphen can be used to indicate a range of continuously numbered references \cite{Bradley2007, Wilford2020, Dhakal2008, Dhakal2001, GNS2014}.
Different formatting styles are used for listing different types of cited references; these are explained at the end of this template.


\subsection{Figure and Table Captions}

Captions for figures and Tables should use the \emph{Figure Caption} and \emph{Table Caption} styles, respectively.
Table captions should be provided above a table, whereas figure captions are provided below a figure.
Both are centrally aligned in \textbf{\textit{bold italics}} 9 points Times New Roman font.
The captions should start with \emph{Table} or \emph{Figure}, followed by the number: and the figure/table title, and end with a full stop; e.g. \textbf{\textit{Figure 1: Test setup.; Table 1: Geometrical properties of the specimens.}}

A 12 points gap should be provided before the Table caption and the preceding text, and a 6 points gap between the caption and the Table.
Similarly, a 6 points gap is provided between a figure and its caption and a 12 points gap between a figure caption and the following text/heading.
If required, a footnote should be typed under a Table in size 8 font with respectively 3 and 12 points spacing above and below it.


\section{Graphics}

Figures need to be provided close to the text introducing them.
They could either occupy a column or cover the full width of the page.
Photographs and relatively simple line-art diagrams may usually be placed within a single column.
However, it may be easier to control the layout of the paper by grouping a number of smaller figures (wherever feasible) to create a combined figure occupying the width of a column or full page Where appropriate, the figures should be placed at the top or bottom of a page.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\columnwidth]{figure2}
    \includegraphics[width=\columnwidth]{figure3}
    \includegraphics[width=\columnwidth]{figure4}
    \caption{Screen clips from Microsoft Word for the full width text box used for Figure 3.}
\end{figure}

Placing figures (especially, those which are wider than a single column) in the text can be challenging and frustrating.
There are several methods of placing them.
The most flexible method is to include the figures in a table (or text box) that is positioned relative to the page because this allows the text to flow or reposition itself around the figure automatically.
The layout settings for a text box are shown in Figure 2.
The lower two dialog box pages are displayed using the Advanced button on the top dialog box.

The same layout can be created using section breaks, with the sections alternating between single and two columns, but the text needs to be repositioned manually.
For example, a section break is created here to fit Figure 3.

Single column figures (like Figure 1 and Figure 2) should use the `In line with text' format (top left of Figure 2) so they move with the text and stay above of their caption.
The figures are centred like a normal paragraph.
Tables may be used to place one or more parts of the figure side-by-side.


\subsection{Colour}

If you choose to use coloured graphics, ensure that they are still clear when printed on a black and white printer.
The on-line (pdf) version of the paper will contain the coloured graphics.
Sections of the paper can be printed in colour by negotiation with the Editor.
There is usually a cost for this, so judicious layout can utilise colour on all 4 pages (2 leaves) required by the printing process.


\subsection{Line Art}

Paste line art from other software into the document as a picture (i.e., select `Picture' from the Insert \(\rightarrow\) Paste Special Paste menu in Word) rather than as an editable object.
The diagram can be pasted straight into the document, but this increases the size of the document file.

Keep line art figures as simple as possible.
The minimum lettering size is 8 points.
Lines should preferably be at least 0.2 mm thick.
Make the lettering size and line thickness proportionally larger if the diagram size is to be reduced after it is inserted into the paper.
Figures should not be wider than 170 mm.
Avoid excessive notes and designations.

Black line-art is the best.
Lighter coloured lines may be printed with jagged edges.
Avoid shaded regions, which may not print uniformly, and avoid very light colours, like yellow, which are almost indistinguishable in a white background.

When a figure and its annotations and arrows are in separate text boxes, these should be grouped.
Otherwise, when the main figure is moved the annotations and arrows get left behind.
Avoid putting figures (graphs) which draw their data from other files (imbedded) as these are very challenging to manipulate (unavoidable during final editing) in the Bulletin setting.
Such figures should be pasted as pictures.


\subsection{Scanned Items}

\subsubsection{Photos}

Photos should be scanned so they have a resolution of 200 dpi (e.g. scan them at 400 dpi if you intend to enlarge them to double their original size or 100 dpi if you will reduce them to half their original size).
Their black (saturation) level should be at least 95\%, which gives them a slightly grey appearance when viewed on the screen.
They need to be saved either in a TIFF or an EPS format.


\subsubsection{Line Art}

If diagrams really need to be scanned, please scan them at a resolution of 600 dpi and have the software save them in a TIFF, GIF or PNG format.
The JPG format is only for photographs.
It reduces the quality of line art by smearing lines as it compresses the image.


\subsection{Digital Photographs}

The requirements for these are similar to those for scanned photos.
Don't convert digital photos to TIFF or EPS format though, leave them in their original format (probably JPG or JPEG) to avoid reducing the image quality.


\subsection{Reducing the Document Size}

You may be able to reduce the size of the finished document by compressing and cropping scanned images.
The dialog box used to do this is displayed using the `compress' button on the `Picture' tab of the Format Picture dialog box.
Save a copy of the document before doing this in case the quality is reduced by this process or you need to enlarge the graphics.


\section{Equations}

For equations within a paragraph with no subscripts or superscripts, such as \(\sin\theta = (x + y) z\), use italic letters for variable names to replicate the format used in the Equation Editor.

For other equations, create the equation using the Equation Editor.
Do not paste equations as objects/pictures as there cannot be edited.
Follow the equation with a tab and a sequential equation number between parentheses.
See for example, Equation 1 below:

\begin{equation}
    K_t = \left(1 - \frac{R^2 \tau}{c_a + \nu \tan\delta}\right)^4 k_1
\end{equation}

where \(c_a\) = adhesion; \\
\(\delta\) = friction angle at interface; and \\
\(k_1\) = shear stiffness number.

[Microsoft Word sometimes refuses to save a document containing equations, with a misleading message that the disk is full.
If this happens (usually after an auto-save while you are editing an equation), select every equation in the document and delete the ones that don't display the message ``Double click to edit Microsoft Equation editor 3.0'' in the bottom status bar.
Try saving the document after you delete each of the equations that have been converted into ordinary pictures.]


\section{Tables}

Locate tables close to the first reference to them in the text and number them consecutively.
Place the caption above the table to the same width as the table (\emph{Table caption} style).
Align all headings to the column centres and start the headings with an initial capital.
Indicate units in a line below the heading.
Explanations should be given at the foot of the table.
Use the \emph{Table text} style for the remaining text.
If notes are required, use the following reference marks: *, **, etc. and place the footnotes directly underneath the table.
Use lines sparingly to separate groups of cells from others.
See for example, Table 1.

\begin{table}[htb!]
    \centering
    \caption{Summary of the significant test properties.}
    \begin{tabu}{X[1.2,c]X[1.0,c]X[1.2,c]X[3.0,c]}
        \hline
        \textbf{Specimen*} & \textbf{Ultimate Strength (kN)} & \textbf{Initial Stiffness (kN/mm)} & \textbf{Earthquake Record} \\
        \hline
        Interior & 27 & 5.7 & 1.25 \(\times\) El-Centro 1940 \\
        Exterior & 14 & 3.0 & NZS 4203 Matahina \\
        \hline
    \end{tabu}
    \vspace{4pt}
    *\emph{\footnotestyle Labelled according to its position in the building}
\end{table}


\section{Final Notes}

Please follow the guidelines as closely as possible to ensure your paper isn't held up because it does not have the correct format.
For convenience, the major formatting features are listed in Table 2.

Avoid changing margins, styles and fonts, particularly when pasting material from other documents.
If you need to do this, it is often better to paste ``unformatted text'' from the Insert \(\rightarrow\) Paste Special menu.


\section{Acknowledgments}

To be provided (as appropriate) only if needed.
An example is provided below.

The authors are thankful to Mr Les Megget and Dr Liam Wotherspoon from University of Auckland; and Dr Bruce Deam from Knowledge Exchange Ltd. for reviewing this template.


\section{References}

All references cited in the text must be listed in the reference, and all references listed must be cited in the text.
The listed references must be numbered and sequenced.
The lists of the references should be in the same sequence as cited in the text.

Names of all co-authors must be written for all references (i.e. writing just the first author's name followed by \emph{et.\ al.} is not allowed).
An author's name should be written as (surname, initials of first and middle names; e.g., Smith, J.B.).
The first letter of all words should be capital when writing the title of the papers and names of a journal/conference.
Formatting details of different types of reference are summarized below.


\subsection{Journal Articles (Example 1 in the list below)}

Surname1 Initials1, Surname2 Initials2 and Surname3 Initials3 (Year). ``Title of Journal Article''. \textit{Journal name}, \textbf{Vol}(Issue): page start-page finish.


\subsection{Conference Papers (Example 2 in the list below)}

Surname1 Initials1, Surname2 Initials2 and Surname3 Initials3 (Year). ``Title of Journal Article''. \textit{Proceedings of Conference Name}, City, Date, (Vol., if applicable), Paper ID or page numbers.


\subsection{Personal Reports (Example 3 in the list below)}

Surname1 Initials1, Surname2 Initials2 and Surname3 Initials3 (Year). ``Title of Report''. Report ID, Publishers’ Employer, City, Length of Report (in terms of number of pages).


\subsection{Institutional Reports/Standards (Example 4 in the list below)}

Publishing Institution (Year). ``\textit{Title of Report/Standard}''. Report/Standard ID, Reporting Institute, City, Length of Report (in terms of number of pages).


\subsection{Theses (Example 5 in the list below)}

Surname Initials (Year). ``\textit{Title of Thesis}''. Masters Thesis OR PhD Dissertation, University name, City, Country, Length of Thesis (in terms of number of pages).


\subsection{Books (Example 6 in the list below)}

Surname1 Initials1, Surname2 Initials2 and Surname3 Initials3 (Year). ``\textit{Title of Book}''. Edition (if applicable), Publisher, City, Length of Book (in terms of number of pages).


\subsection{Book Chapters (Example 7 in the list below)}

Surname1 Initials1, Surname2 Initials2 and Surname3 Initials3 (Year). ``\textit{Title of Chapter}'' in Title of Book. Publisher, City, Length of Chapter (in terms of number of pages).


\subsection{Online Information (Example 8 in the list below)}

Publisher (Year). \textit{Title of Information (if applicable)}. Website address, (accessed date).

\begin{table*}
    \centering
    \caption{Summary of formatting guidelines}
    \begin{tabu}{|X[1.7,l,m]|X[0.4,c,m]|X[1,c,m]|X[1,c,m]|X[1.8,c,m]|X[1,c,m]|}
        \hline
        & \textbf{Font Size} & \textbf{Emphasis} & \textbf{Capital/Small} & \textbf{Orientation} & \textbf{Spacing Before and After} \\
        \hline
        Title & 14 & \textbf{BOLD} & \textbf{CAPITAL} & \textbf{CENTRE OF PAGE} & 6, 18 \\
        \hline
        Authors & 14 & \textbf{Bold} & \textbf{First Letter Capital} & \textbf{Centre of Page} & 12, 12 \\
        \hline
        Milestone dates & 9 & Partially \textit{italics} & All Words Start with Capital & Centre of Page & 12, 12 \\
        \hline
        Abstract & 9 & None & Normal & Align left and right (single column) & 6, 6 (1 blank line at the end) \\
        \hline
        Affiliation (1st page footer) & 8 & \textit{Italics} & \textit{All Words Start with Capital} & \textit{Align superscripted numbers to the left (indent text)} & 0, 0 \\
        \hline
        First heading: (e.g. Introduction) & 9 & \textbf{BOLD} & \textbf{CAPITAL} & \textbf{CENTRE OF COLUMN} & 6, 6 \\
        \hline
        Other Main headings* & 9 & \textbf{BOLD} & \textbf{CAPITAL} & \textbf{CENTRE OF COLUMN} & 12, 6 \\
        \hline
        Sub-headings & 9 & \textbf{Bold} & \textbf{All Words Start with Capital} & \textbf{Align to the Left} & 12, 6 \\
        \hline
        3rd level headings & 9 & \textit{Italics} & \textit{All Words Start with Capital} & \textit{Align to th e Left} & 12, 6 \\
        \hline
        4th level headings & 9 & None & Normal & Align to the Left & 12, 6 \\
        \hline
        Main text (including Figures and Tables) & 9 & None & Normal & Align left and right (double column) & 6, 6 \\
        \hline
        Figure Captions & 9 & \textbf\textit{{Bold Italics}} & \textbf{\textit{Normal}} & \textbf{\textit{Center of figure width}} & 6, 12 \\
        \hline
        Table Captions & 9 & \textbf\textit{{Bold Italics}} & \textbf{\textit{Normal}} & \textbf{\textit{Center of the Table width}} & 12, 6 \\
        \hline
        Text inside Table & 8 or 9 & As required & As required & As required & 3, 3 \\
        \hline
        Table Footnote & 8 & \textit{Italics} & \textit{Normal} & \textit{Centre of the Table width} & 3, 12 \\
        \hline
        Numbered/Bulleted lists in text & 9 & None & Normal & Align numbers/bullets to the left (indent text) & 3, 3 \\
        \hline
        References & 9 & Partially \textit{italics} & Normal & Align numbers to the left (indent text) & 3, 3 \\
        \hline
    \end{tabu}
    \vspace{4pt}
    *\emph{\footnotestyle Also applies to ABSTRACT, ACKNOWLEDGMENT, REFERENCES, and APPENDIX.}
\end{table*}

\nocite{*} % NOTE: This command lists all references in the specified bibliography file, irrespective of whether they are actually cited in the manuscript. Do not use it when preparing your manuscript.
\bibliographystyle{nzsee}
\vspace{-20pt}
\bibliography{references}


\end{document}
