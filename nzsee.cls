% -----------------------------------------------------------------------------------------------------------
% 
% This LaTeX class file is intended to help authors prepare manuscripts for submission to the Bulletin of
% the New Zealand Society for Earthquake Engineering.
% 
% You are NOT permitted to modify this file.
%
% Using the `preparation' option switches on line numbering in the centre margin. This permits the placement
% of notes in the left and right margins, which may be useful during manuscript preparation.
% 
% Using the `review' option switches on line numbering in the left and right margins. This is the recommended
% formatting option to use when submitting a manuscript for review.
% 
% Created by
%    Reagan Chandramohan
%    Lecturer
%    Department of Civil and Natural Resources Engineering
%    University of Canterbury
% 
% for
%    The New Zealand Society for Earthquake Engineering
% 
% Version 2.0
% 14-May-2022
% 
% -----------------------------------------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{nzsee}[2017/03/01 NZSEE]

\LoadClass[a4paper,twoside]{article}
\RequirePackage[T1]{fontenc}
\RequirePackage{mathptmx,bm,graphicx,amsmath,changepage,titlesec,caption,fix-cm,fancyhdr,environ,microtype,afterpage,etoolbox,calc}
\RequirePackage[hyphens]{url}
%\RequirePackage[colorlinks,allcolors=blue]{hyperref}
\RequirePackage[hang,flushmargin,ragged,norule]{footmisc}
\RequirePackage[
    top=19.5mm,
    bottom=20mm,
    left=20mm,
    right=20mm,
    headsep=3mm,
    marginparsep=1.2mm,
    marginparwidth=18.5mm
]{geometry}
\RequirePackage{flushend}

% Define macros
\newcommand\volnum{XX}
\newcommand\issuenum{Y}
\newcommand\pubmonth{Month}
\newcommand\pubyear{Year}
\newcommand\submitmonth{Month}
\newcommand\submityear{Year}
\newcommand\reviewmonth{Month}
\newcommand\reviewyear{Year}
\newcommand\acceptmonth{Month}
\newcommand\acceptyear{Year}

% Define font sizes and spacing
\newcommand{\titlestyle}[1]{\fontsize{14}{16pt}\selectfont\begin{adjustwidth}{16.3mm}{16.3mm}\centering\bfseries\MakeUppercase{#1}\end{adjustwidth}}
\newcommand{\authorstyle}[1]{\fontsize{14}{16pt}\selectfont\begin{adjustwidth}{16.3mm}{16.3mm}\centering\bfseries#1\end{adjustwidth}}
\newcommand{\footnotestyle}{\fontsize{8}{10pt}\selectfont}
\renewcommand{\normalsize}{\fontsize{9}{11pt}\selectfont}

% Define title and author formatting
\renewcommand{\@maketitle}{%
    \vspace{13.2mm}%
    \titlestyle{\@title}\par%
    \vspace{13pt}%
    \authorstyle{\@author}\par%
    \vspace{11pt}%
    \centering\normalsize%
    (Submitted \textit{\submitmonth{} \submityear{}}; Reviewed \textit{\reviewmonth{} \reviewyear{}}; Accepted \textit{\acceptmonth{} \acceptyear{}})\par%
    \vspace{6pt}%
}

% Define author affiliation formatting
\newlength{\footerexpand}
\newcommand{\affilnum}[1]{\textsuperscript{\footnotesize#1}}
\newcommand{\affiladdrs}{}
\newcommand{\affiladdr}[2]{%
    \appto{\affiladdrs}{\textsuperscript{#1} & \emph{#2} \\}%
    \setlength{\footerexpand}{\footerexpand - 5.2mm}%
}

% Define the header for the first and remaining pages
\fancypagestyle{plain}{
    \fancyhead[LO,RE]{}
    \fancyhead[CO,CE]{\itshape Bulletin of the New Zealand Society for Earthquake Engineering, Vol.\ \volnum{}, No.\ \issuenum{}, \pubmonth{} \pubyear{}}
    \fancyhead[RO,LE]{\thepage\vspace{13pt}}
    \fancyfoot{}
    \lfoot{%
        \vspace{\footerexpand}%
        \footnotestyle%
        \tabcolsep 1pt%
        \begin{tabular}{l p{0.98\textwidth}}%
            \affiladdrs{}%
        \end{tabular}%
    }
    \renewcommand{\headrulewidth}{0pt}
}

\pagestyle{fancy}
\fancyhead[LO,RE]{}
\fancyhead[CO,CE]{}
\fancyhead[RO,LE]{\thepage\vspace{16.5pt}}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}

% Define abstract formatting
\RenewEnviron{abstract}{%
    \twocolumn[%
        \maketitle%
        \begin{adjustwidth}{16.3mm}{16.3mm}%
            \section{Abstract}%
            \setlength{\parskip}{6pt}%
            \BODY%
            \vspace{22pt}%
        \end{adjustwidth}%
    ]%
    \enlargethispage{\footerexpand}%
    \afterpage{\enlargethispage{\footerexpand}}%
}

% Define section title formatting
\titlelabel{}
\titleformat*{\section}{\centering\bfseries\uppercase}
\titlespacing*{\section}{0pt}{6pt}{0pt}
\titleformat*{\subsection}{\bfseries}
\titlespacing*{\subsection}{0pt}{6pt}{0pt}
\titleformat*{\subsubsection}{\itshape}
\titlespacing*{\subsubsection}{0pt}{6pt}{0pt}
\titleformat{\paragraph}{}{}{}{}
\titlespacing*{\paragraph}{0pt}{6pt}{0pt}

% Define figure and table caption formatting
\DeclareCaptionFormat{capformat}{\fontsize{9}{11pt}\selectfont\bfseries\itshape#1#2#3}
\captionsetup{format=capformat,justification=centering}
\DeclareCaptionFormat{subcapformat}{\fontsize{9}{11pt}\selectfont\itshape#1#2#3}
\captionsetup[sub]{format=subcapformat,justification=centering}

% Define the list styles
\RequirePackage{enumitem}
\setlist[enumerate,itemize]{align=parleft,leftmargin=!,labelwidth=0.6cm,labelsep=0cm}

% Define paragraph indent and spacing
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}
\setlength{\columnsep}{10mm}

% Define a conditional to check if the 'preparation' or 'review' options are passed to the documentclass
\newif\ifpreparation
\newif\ifreview
\makeatletter
\@ifclasswith{nzsee}{preparation}{\preparationtrue}{\preparationfalse}
\@ifclasswith{nzsee}{review}{\reviewtrue}{\reviewfalse}
\makeatother

% If 'preparation' is set, enable line numbers in between the two columns of text to allow margin notes to be
% placed in the regular margins
\ifpreparation
    \RequirePackage[switch*]{lineno}
    \linenumbers
    \setlength\linenumbersep{1.5mm}
    \setlength\linenumberwidth{2.8mm}
    \linenumfix{}
\fi

% If 'review' is set, enable line numbers using default settings
\ifreview
    \RequirePackage{lineno}
    \linenumbers
    \linenumfix{}
\fi

% Prevent line numbers from not showing in paragraphs preceding math environments like 'align'
% Uses recommendations from http://phaseportrait.blogspot.com/2007/08/lineno-and-amsmath-compatibility.html
\newcommand*\patchAmsMathEnvironmentForLineno[1]{%
    \expandafter\let\csname old#1\expandafter\endcsname\csname #1\endcsname%
    \expandafter\let\csname oldend#1\expandafter\endcsname\csname end#1\endcsname%
    \renewenvironment{#1}{\linenomath\csname old#1\endcsname}{\csname oldend#1\endcsname\endlinenomath}%
}
\newcommand*\patchBothAmsMathEnvironmentsForLineno[1]{%
    \patchAmsMathEnvironmentForLineno{#1}%
    \patchAmsMathEnvironmentForLineno{#1*}%
}
\newcommand\linenumfix{%
    \AtBeginDocument{%
        \patchBothAmsMathEnvironmentsForLineno{equation}%
        \patchBothAmsMathEnvironmentsForLineno{align}%
        \patchBothAmsMathEnvironmentsForLineno{flalign}%
        \patchBothAmsMathEnvironmentsForLineno{alignat}%
        \patchBothAmsMathEnvironmentsForLineno{gather}%
        \patchBothAmsMathEnvironmentsForLineno{multline}%
    }%
}

% Define bibliography formatting
\RequirePackage[square,comma,numbers,sort&compress]{natbib}
\renewcommand{\refname}{\uppercase{References}}
\setlength{\bibsep}{3pt}
\makeatletter 
\renewcommand\@biblabel[1]{#1\hfill} 
\makeatother

% Handle special characters
\RequirePackage[utf8]{inputenc}

% Define URL and DOI formatting
\newcommand{\urlprefix}{}
\renewcommand\UrlFont{\rmfamily}

% Remove the space after a comma in a citation list
\newlength\mylen
\settowidth\mylen{\space}
\setcitestyle{citesep={,\kern-\mylen}}

% Minimise occurrences of lines running into margins
\tolerance=1000
\setlength{\emergencystretch}{2em}
